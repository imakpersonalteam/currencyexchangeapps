# CurrencyExchangeApps

An example of iOS application based on currency exchange [API](https://fixer.io) using MVP+Coordinator+Rx design pattern and Core Data (SQLite) to persist data.

The app helps to keep tracking currency courses. Just open the app and select **Base** currency (only EUR for free subscription plan), **Target** currencies, and start typing amount. All selected targets persist on the device and currency exchange info will be updated automatically even if the app is closed.