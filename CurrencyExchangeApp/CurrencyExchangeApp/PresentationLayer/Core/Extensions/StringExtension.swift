//
//  StringExtension.swift
//  CurrencyConverter
//
//  Created by ilya on 17/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

extension String {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
