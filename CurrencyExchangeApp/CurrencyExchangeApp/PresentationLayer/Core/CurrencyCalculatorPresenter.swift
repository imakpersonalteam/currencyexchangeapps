//
//  CurrencyCalculatorPresenter.swift
//  CurrencyConverter
//
//  Created by ilya on 6/11/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

class CurrencyCalculatorPresenter: CurrencyCalculatorPresenterProtocol {

    weak var view: CurrencyCalculatorView?

    var model: CurrencyRepository
    var onOpenTargetsScreen: (([Symbol]) -> Void)?

    private let disposeBag = DisposeBag()

    private var baseCurrencyUIModel: CurrencyCalculatorModel!
    private var targetCurrencies: [Symbol] = []
    private var targetCurrenciesUIModels: [CurrencyCalculatorModel] = []

    private lazy var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.alwaysShowsDecimalSeparator = true
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.minimumIntegerDigits = 2
        return formatter
    }()

    init(base: Currency, targets: [Currency], view: CurrencyCalculatorView, model: CurrencyRepository) {
        self.baseCurrencyUIModel = CurrencyCalculatorModel(symbol: base.symbol, valueRate: 0, value: "00.00")
        self.targetCurrencies = targets.map { $0.symbol }
        self.view = view
        self.model = model
    }

    init(base: Currency, rates: [CurrencyRate], view: CurrencyCalculatorView, model: CurrencyRepository) {
        self.baseCurrencyUIModel = CurrencyCalculatorModel(symbol: base.symbol, valueRate: 0, value: "00.00")
        self.view = view
        self.model = model

        rates.forEach { rate in
            targetCurrencies.append(rate.symbol)

            let valueRate = Decimal(rate.value)
            let baseCurrencyValue = baseCurrencyUIModel.valueRate
            let calculatedValue = baseCurrencyValue * valueRate
            let formattedValue = numberFormatter.string(from: calculatedValue as NSNumber)!
            targetCurrenciesUIModels.append(CurrencyCalculatorModel(symbol: rate.symbol,
                                                                    valueRate: valueRate,
                                                                    value: formattedValue))
        }
    }

    func viewDidLoad() {
        view?.title = "CalculatorKey".localized
        view?.show(baseCurrency: baseCurrencyUIModel)

        if !targetCurrencies.isEmpty {
            if targetCurrenciesUIModels.isEmpty {
                loadRates()
            } else {
                view?.show(targets: targetCurrenciesUIModels)
            }
        }
    }

    func didTapOnBaseCurrency() {
        // unavailable for FREE subscription plan
    }

    func didTapNewTarget() {
        onOpenTargetsScreen?(targetCurrencies)
    }

    func add(newTargets: [Currency]) {
        if !newTargets.isEmpty {
            targetCurrencies.append(contentsOf: newTargets.map { $0.symbol })
            loadRates()
        }
    }

    func didTapDelete(target: CurrencyCalculatorModel, at index: Int) {
        // fast operation, it is not necessary wait the end of deletion because we control ui data directly
        _ = model.delete(targetsRates: [target.symbol], for: baseCurrencyUIModel.symbol)
            .subscribe()

        targetCurrencies.removeAll { $0 == target.symbol }
        targetCurrenciesUIModels.remove(at: index)
        view?.updateTargets(animated: true)
    }

    func didChangeBaseCurrency(value: String?) {
        if let value = value, let number = Decimal(string: value) {
            baseCurrencyUIModel.value = value
            baseCurrencyUIModel.valueRate = number
            targetCurrenciesUIModels.forEach {
                let decimalValue = number * $0.valueRate
                $0.value = numberFormatter.string(from: decimalValue as NSNumber)!
            }
        }
        view?.updateTargets(animated: false)
    }

    func format(baseCurrencyValue: String?) -> String {
        var value = baseCurrencyValue ?? ""

        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        value = regex.stringByReplacingMatches(in: value,
                                               options: .reportProgress,
                                               range: NSMakeRange(0, value.count),
                                               withTemplate: "")
        let decimalValue = (Decimal(string: value) ?? 0) / 100
        return numberFormatter.string(from: decimalValue as NSNumber)!
    }

    private func loadRates() {
        let baseValue = baseCurrencyUIModel.valueRate
        let formatter = numberFormatter
        model.getRates(for: baseCurrencyUIModel.symbol, to: targetCurrencies)
            .map { rates -> [CurrencyCalculatorModel] in
                rates.map {
                    let rateValue = Decimal($0.value)
                    let calculatingRate = rateValue * baseValue
                    let formattedValue = formatter.string(from: calculatingRate as NSNumber)!
                    return CurrencyCalculatorModel(symbol: $0.symbol, valueRate: rateValue, value: formattedValue)
                }
            }
            .observeOn(MainScheduler.instance)
            .do(onSuccess: { [weak self] in
                self?.targetCurrenciesUIModels = $0
                self?.view?.show(targets: $0)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
}
