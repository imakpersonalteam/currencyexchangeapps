//
//  CoreContracts.swift
//  CurrencyConverter
//
//  Created by ilya on 6/11/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

protocol CoreView: class {

    var title: String? {get set}
}

protocol CorePresenter: class {

    var model: CurrencyRepository {get set}

    func viewDidLoad()
}
