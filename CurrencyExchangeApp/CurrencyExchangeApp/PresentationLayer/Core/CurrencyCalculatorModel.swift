//
//  CurrencyCalculatorModel.swift
//  CurrencyConverter
//
//  Created by ilya on 6/11/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

class CurrencyCalculatorModel {

    let symbol: String
    
    var valueRate: Decimal
    var value: String

    init(symbol: String, valueRate: Decimal, value: String) {
        self.symbol = symbol
        self.valueRate = valueRate
        self.value = value
    }
}
