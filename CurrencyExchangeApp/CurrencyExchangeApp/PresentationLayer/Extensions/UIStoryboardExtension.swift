//
//  UIStoryboardExtension.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

extension UIStoryboard {

    public class func instantiateViewController <T: UIViewController>(type: T.Type,
                                                                      storyboardIdentifier: String) -> T {
        let storyboard = UIStoryboard(name: storyboardIdentifier, bundle: nil)
        return storyboard.instantiateViewController(type: type)
    }

    public func instantiateViewController <T: UIViewController>(type: T.Type) -> T {
        return instantiateViewController(withIdentifier: String(describing: type)) as! T
    }
}
