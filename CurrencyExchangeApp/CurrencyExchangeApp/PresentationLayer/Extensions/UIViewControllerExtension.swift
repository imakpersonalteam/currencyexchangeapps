//
//  UIViewControllerExtension.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

extension UIViewController {

    public class func instantiateFromStoryboard(storyboardName: String) -> Self {
        return UIStoryboard.instantiateViewController(type: self, storyboardIdentifier: storyboardName)
    }
}
