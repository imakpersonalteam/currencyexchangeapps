//
//  CalculatorCoordinator.swift
//  CurrencyConverter
//
//  Created by ilya on 6/13/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

class CalculatorCoordinator {

    weak var navigationController: UINavigationController?

    var onTargetsSelected: (([Currency]) -> Void)?

    private var dataModel: CurrencyRepository

    init(navigationController: UINavigationController?, dataModel: CurrencyRepository) {
        self.navigationController = navigationController
        self.dataModel = dataModel
    }

    func openTargetCurrencyPicker(excludeCurrencies: [Symbol]) {
        let view = CurrencyPickerViewController.newInstance()
        let presenter = TargetsCurrencyPickerPresenter(exclude: excludeCurrencies, view: view, model: dataModel)
        view.presenter = presenter

        presenter.onTargetsCurrencySelected = { [weak self] currencies in
            self?.onTargetsSelected?(currencies)
            self?.navigationController?.popViewController(animated: true)
        }

        navigationController?.pushViewController(view, animated: true)
    }
}
