//
//  RootCoordinator.swift
//  CurrencyConverter
//
//  Created by ilya on 09/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

class RootCoordinator {

    private weak var window: UIWindow?

    private lazy var dataModel = CurrencyDataProvider(dataLoader: CurrencyNetworkProvider(),
                                                      dbLoader: CurrencyDbDataProvider())
    private lazy var calculatorCoordinator: CalculatorCoordinator = {
        return CalculatorCoordinator(navigationController: window?.rootViewController as? UINavigationController,
                                     dataModel: dataModel)
    }()

    private var currencyPickerView: CurrencyPickerViewController {
        return CurrencyPickerViewController.newInstance()
    }

    init(window: UIWindow?) {
        self.window = window
    }

    func startBaseCurrencyPicker() {
        let view = currencyPickerView
        let presenter = BaseCurrencyPickerPresenter(view: view, model: dataModel)
        view.presenter = presenter

        presenter.onBaseCurrencySelected = { [weak self] in
            self?.openTargetsCurrenciesPicker(baseCurrency: $0)
        }

        window?.rootViewController = UINavigationController(rootViewController: view)
    }

    private func openTargetsCurrenciesPicker(baseCurrency: Currency) {
        let view = currencyPickerView
        let presenter = TargetsCurrencyPickerPresenter(exclude: [baseCurrency.symbol], view: view, model: dataModel)
        view.presenter = presenter
        
        presenter.onTargetsCurrencySelected = { [weak self] targets in
            self?.startCurrencyCalculator(baseCurrency: baseCurrency, targetsCarrencies: targets)
        }
        (window?.rootViewController as? UINavigationController)?.pushViewController(view, animated: true)
    }

    func startCurrencyCalculator(baseCurrency: Currency, targetsRates: [CurrencyRate]) {
        let view = CurrencyCulculatorViewController.newInstance()
        let presenter = CurrencyCalculatorPresenter(base: baseCurrency,
                                                    rates: targetsRates,
                                                    view: view,
                                                    model: dataModel)
        view.presenter = presenter

        presenter.onOpenTargetsScreen = { [weak self] pickedCurrencies in
            self?.calculatorCoordinator.openTargetCurrencyPicker(excludeCurrencies: pickedCurrencies)
            self?.calculatorCoordinator.onTargetsSelected = { presenter.add(newTargets: $0) }
        }

        window?.rootViewController = UINavigationController(rootViewController: view)
    }

    func startCurrencyCalculator(baseCurrency: Currency, targetsCarrencies: [Currency]) {
        let view = CurrencyCulculatorViewController.newInstance()
        let presenter = CurrencyCalculatorPresenter(base: baseCurrency,
                                                    targets: targetsCarrencies,
                                                    view: view,
                                                    model: dataModel)
        view.presenter = presenter

        presenter.onOpenTargetsScreen = { [weak self] pickedCurrencies in
            self?.calculatorCoordinator.openTargetCurrencyPicker(excludeCurrencies: pickedCurrencies)
            self?.calculatorCoordinator.onTargetsSelected = { presenter.add(newTargets: $0) }
        }

        window?.rootViewController = UINavigationController(rootViewController: view)
    }
}
