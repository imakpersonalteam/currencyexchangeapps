//
//  AppDelegate.swift
//  CurrencyConverter
//
//  Created by ilya on 01/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private let syncTimeInterval: TimeInterval = (3600 * 24) + 20 // Time Interval for closest update time (hourly update for FREE subscription plan)
    private let disposeBag = DisposeBag()

    private lazy var rootCoordinator: RootCoordinator = RootCoordinator(window: window)


    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let dataModel = CurrencyDataProvider(dataLoader: CurrencyNetworkProvider(), dbLoader: CurrencyDbDataProvider())
        let currencyDataModel: Single<(base: Currency, targetsRates: [CurrencyRate])?>

        // check if a user grants access to fetch data in background mode
        if application.backgroundRefreshStatus == .available {
            /* set fetch data interval once a day. The system calls method for fetching data automatically based on time interval in apropriate time.
             There is no guarantee that the method will be called in exact time, it depends on system settings, battary life, resources ect.
             */
            application.setMinimumBackgroundFetchInterval(syncTimeInterval)

            currencyDataModel =  dataModel.getDefaultCurrency()
        } else {
            currencyDataModel = dataModel
                .sync()
                .flatMap { dataModel.getDefaultCurrency() }
        }
        currencyDataModel
            .observeOn(MainScheduler.instance)
            .do(onSuccess: { [weak self] in
                if let defaults = $0  {
                    self?.rootCoordinator.startCurrencyCalculator(baseCurrency: defaults.base,
                                                                  targetsRates: defaults.targetsRates)
                } else {
                    self?.rootCoordinator.startBaseCurrencyPicker()
                }
            })
            .subscribe()
            .disposed(by: disposeBag)
        return true
    }

    func application(_ application: UIApplication,
                     performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        _ = CurrencyDataProvider(dataLoader: CurrencyNetworkProvider(), dbLoader: CurrencyDbDataProvider())
            .sync()
            .do(onSuccess: ({ _ in completionHandler(.newData) }),
                onError: ({ _ in completionHandler(.failed) }))
            .subscribe()
    }
}

