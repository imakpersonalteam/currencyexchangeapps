//
//  CurrencyPickerModel.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

struct CurrencyPickerModel {
    let symbol: String
    let title: String
    var isSelected: Bool
}
