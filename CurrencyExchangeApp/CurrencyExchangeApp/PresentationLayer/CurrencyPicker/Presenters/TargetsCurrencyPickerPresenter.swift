//
//  TargetsCurrencyPickerPresenter.swift
//  CurrencyConverter
//
//  Created by ilya on 09/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

class TargetsCurrencyPickerPresenter: CurrencyPickerPresenter {

    weak var view: CurrencyPickerView?
    var model: CurrencyRepository

    var onTargetsCurrencySelected: (([Currency]) -> Void)?

    private let disposeBag = DisposeBag()

    private let excludedCurrencies: [Symbol]?
    private var targetsBySymbol: [Symbol : Currency] = [:]
    private var selectedTargets: [Symbol : Currency] = [:]

    init(exclude: [Symbol]?, view: CurrencyPickerView, model: CurrencyRepository) {
        self.view = view
        self.model = model
        self.excludedCurrencies = exclude
    }

    func viewDidLoad() {
        view?.title = "PickTargetCurrencyTitleKey".localized
        view?.setupCompletionAction()
        loadCurrencies(filterText: nil)
    }

    func didSelect(currency: CurrencyPickerModel) {
        selectedTargets[currency.symbol] = targetsBySymbol[currency.symbol]!
    }

    func didDeselect(currency: CurrencyPickerModel) {
        selectedTargets.removeValue(forKey: currency.symbol)
    }

    func didSelectionComplete() {
        onTargetsCurrencySelected?(Array(selectedTargets.values))
    }

    func didFileter(text: String?) {
        loadCurrencies(filterText: text)
    }

    private func loadCurrencies(filterText: String?) {
        let excluded = excludedCurrencies
        let targets = selectedTargets

        model.getAvailableCurrencies(filtered: filterText)
            .map { [weak self] targets -> [Currency] in
                var targets = targets
                if let excluded = excluded {
                    targets.removeAll { excluded.contains($0.symbol) }
                }
                self?.targetsBySymbol = targets.toDictionary { $0.symbol }
                return targets
            }
            .map { $0.map {CurrencyPickerModel(symbol: $0.symbol, title: $0.name, isSelected: targets[$0.symbol] != nil)}}
            .observeOn(MainScheduler.instance)
            .do(onSuccess: { [weak self] currencies in self?.view?.show(currencies: currencies) })
            .subscribe()
            .disposed(by: disposeBag)
    }
}
