//
//  BaseCurrencyPickerPresenter.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

class BaseCurrencyPickerPresenter: CurrencyPickerPresenter {

    weak var view: CurrencyPickerView?

    var model: CurrencyRepository
    var onBaseCurrencySelected: ((Currency) -> Void)?

    private let disposeBag = DisposeBag()

    private var allCurrencies: [Currency]!

    init(view: CurrencyPickerView, model: CurrencyRepository) {
        self.view = view
        self.model = model
    }

    func viewDidLoad() {
        view?.title = "PickBaseCurrencyTitleKey".localized
        loadCurrencies(filterText: nil)
    }

    func didSelect(currency: CurrencyPickerModel) {
        // Even if an error occurces, user will abel to convert the currency. The only difference is a user will pick base currency once again
        let selectedCurrency = allCurrencies.first { $0.symbol == currency.symbol }!
        model.makeDefault(baseCurrency: currency.symbol)
            .observeOn(MainScheduler.instance)
            .subscribe() { [weak self] _ in
                self?.onBaseCurrencySelected?(selectedCurrency)
            }
            .disposed(by: disposeBag)
    }

    func didDeselect(currency: CurrencyPickerModel) {}

    func didSelectionComplete() {}

    func didFileter(text: String?) {
        loadCurrencies(filterText: text)
    }

    private func loadCurrencies(filterText: String?) {
        model.getAvailableCurrencies(filtered: filterText)
            .do(onSuccess: { [weak self] in self?.allCurrencies = $0 })
            .map {
                $0
                    .filter { $0.symbol == "EUR" } // only EUR base currency is available for FREE subscription plan
                    .map { CurrencyPickerModel(symbol: $0.symbol, title: $0.name, isSelected: false) }
            }
            .observeOn(MainScheduler.instance)
            .do(onSuccess: { [weak self] currencies in
                self?.view?.infoMessage = "BaseCurrencyRestrictionMessageKey".localized
                self?.view?.show(currencies: currencies)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
}
