//
//  CurrencyPickerContracts.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

protocol CurrencyPickerView: CoreView {

    var presenter: CurrencyPickerPresenter! {get set}

    var infoMessage: String? {get set}

    func setupCompletionAction()
    func show(currencies: [CurrencyPickerModel])
}

protocol CurrencyPickerPresenter: CorePresenter {

    var view: CurrencyPickerView? {get set}

    func didSelect(currency: CurrencyPickerModel)
    func didDeselect(currency: CurrencyPickerModel)
    func didFileter(text: String?)
    func didSelectionComplete()
}
