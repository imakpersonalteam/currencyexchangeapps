//
//  CurrencyPickerCell.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

class CurrencyPickerCell: UITableViewCell {

    @IBOutlet private weak var symbolView: UILabel!
    @IBOutlet private weak var nameView: UILabel!

    func bind(symbol: String, name: String) {
        symbolView.text = symbol
        nameView.text = name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
    }
}
