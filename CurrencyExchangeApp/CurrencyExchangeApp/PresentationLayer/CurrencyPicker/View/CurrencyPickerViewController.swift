//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by ilya on 01/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

class CurrencyPickerViewController: UITableViewController, CurrencyPickerView, UISearchResultsUpdating {

    var presenter: CurrencyPickerPresenter!

    var infoMessage: String?

    private var tableData: [CurrencyPickerModel] = []

    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "SearchPlaceHolderKey".localized
        searchController.hidesNavigationBarDuringPresentation = false
        return searchController
    }()

    static func newInstance() -> CurrencyPickerViewController {
        return CurrencyPickerViewController.instantiateFromStoryboard(storyboardName: "Main")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false

        tableView.register(UINib(nibName: CurrencyPickerCell.className, bundle: nil),
                           forCellReuseIdentifier: CurrencyPickerCell.className)
        presenter.viewDidLoad()
    }

    func setupCompletionAction() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(actionDone))
    }

    @objc private func actionDone() {
        presenter.didSelectionComplete()
    }

    func show(currencies: [CurrencyPickerModel]) {
        tableData = currencies
        tableView.reloadData()
    }

    //MARK: - Search controller delegate
    func updateSearchResults(for searchController: UISearchController) {
        presenter.didFileter(text: searchController.searchBar.text)
    }


    //MARK: - Table dataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyPickerCell.className, for: indexPath)
            as! CurrencyPickerCell
        let currency = tableData[indexPath.row]
        cell.bind(symbol: currency.symbol, name: currency.title)

        if currency.isSelected && !cell.isSelected {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UILabel() // we do not need to call tableView.dequeueReusableTableFooterView because of single section
        footer.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        footer.backgroundColor = .groupTableViewBackground
        footer.textAlignment = .center
        footer.text = infoMessage
        footer.numberOfLines = 2
        return footer
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return infoMessage == nil ? 0 : 40
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var item = tableData[indexPath.row]
        item.isSelected = true
        tableData[indexPath.row] = item
        presenter.didSelect(currency: item)
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        var item = tableData[indexPath.row]
        item.isSelected = false
        tableData[indexPath.row] = item
        presenter.didDeselect(currency: tableData[indexPath.row])
    }
}
