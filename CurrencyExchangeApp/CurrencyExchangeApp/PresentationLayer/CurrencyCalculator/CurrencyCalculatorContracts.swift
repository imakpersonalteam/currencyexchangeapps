//
//  CurrencyCalculatorContracts.swift
//  CurrencyConverter
//
//  Created by ilya on 6/11/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

protocol CurrencyCalculatorView: CoreView {

    var presenter: CurrencyCalculatorPresenterProtocol! {get set}

    func show(baseCurrency: CurrencyCalculatorModel)
    func show(targets: [CurrencyCalculatorModel])
    func updateTargets(animated: Bool)
}

protocol CurrencyCalculatorPresenterProtocol: CorePresenter {

    var view: CurrencyCalculatorView? {get set}

    func didTapOnBaseCurrency()
    func didTapNewTarget()
    func didTapDelete(target: CurrencyCalculatorModel, at index: Int)
    func didChangeBaseCurrency(value: String?)
    func add(newTargets: [Currency])
    func format(baseCurrencyValue: String?) -> String
}
