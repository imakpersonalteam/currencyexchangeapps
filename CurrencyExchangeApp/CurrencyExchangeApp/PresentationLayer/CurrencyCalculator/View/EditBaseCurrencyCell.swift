//
//  EditBaseCurrencyCell.swift
//  CurrencyConverter
//
//  Created by ilya on 10/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

protocol EditBaseCurrencyCellDelegate: class {
    func didTapCurrencyAction()
    func didChangeCurrency(sender: UITextField, value: String?)
}
class EditBaseCurrencyCell: UITableViewCell {

    weak var delegate: EditBaseCurrencyCellDelegate?

    @IBOutlet private weak var editView: UITextField!
    @IBOutlet private weak var currencyView: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        currencyView.addTarget(self, action: #selector(didTapCurrencyView), for: .touchUpInside)
        editView.addTarget(self, action: #selector(editingChanged(view:)), for: .editingChanged)
    }

    func bind(symbol: String, value: String) {
        editView.text = value
        currencyView.setTitle(symbol, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if !editView.isFirstResponder && selected {
            editView.becomeFirstResponder()
        }
    }

    @objc private func editingChanged(view: UITextField) {
        delegate?.didChangeCurrency(sender: view, value: view.text)
    }

    @objc private func didTapCurrencyView() {
        delegate?.didTapCurrencyAction()
    }
}
