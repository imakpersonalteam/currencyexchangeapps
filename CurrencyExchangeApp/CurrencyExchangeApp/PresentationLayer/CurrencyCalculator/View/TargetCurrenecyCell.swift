//
//  TargetCurrenecyCell.swift
//  CurrencyConverter
//
//  Created by ilya on 6/11/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

class TargetCurrenecyCell: UITableViewCell {
    
    @IBOutlet private weak var valueView: UILabel!
    @IBOutlet private weak var symbolView: UILabel!

    func bind(symbol: String, value: String) {
        symbolView.text = symbol
        valueView.text = value
    }
}
