//
//  CurrencyCulculatorViewController.swift
//  CurrencyConverter
//
//  Created by ilya on 09/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import UIKit

private enum CellType {
    case baseCurrency
    case targetCurrency
    case addTargetCurrency

    var reuseIdentifier: String {
        switch self {
        case .baseCurrency:
            return EditBaseCurrencyCell.className
        case .targetCurrency:
            return TargetCurrenecyCell.className
        case .addTargetCurrency:
            return UITableViewCell.className
        }
    }
}
class CurrencyCulculatorViewController: UITableViewController, CurrencyCalculatorView, EditBaseCurrencyCellDelegate {

    var presenter: CurrencyCalculatorPresenterProtocol!

    private weak var baseCurrency: CurrencyCalculatorModel?
    private var targetsCurrencies: [CurrencyCalculatorModel] = []

    private let tableData: [CellType] = [.baseCurrency , .targetCurrency, .addTargetCurrency]

    static func newInstance() -> CurrencyCulculatorViewController {
        return CurrencyCulculatorViewController.instantiateFromStoryboard(storyboardName: "Main")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelectionDuringEditing = true
        tableView.register(UINib(nibName: EditBaseCurrencyCell.className, bundle: nil),
                           forCellReuseIdentifier: CellType.baseCurrency.reuseIdentifier)
        tableView.register(UINib(nibName: TargetCurrenecyCell.className, bundle: nil),
                           forCellReuseIdentifier: CellType.targetCurrency.reuseIdentifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellType.addTargetCurrency.reuseIdentifier)

        presenter.viewDidLoad()
    }

    func didTapCurrencyAction() {
        presenter.didTapOnBaseCurrency()
    }

    func didChangeCurrency(sender: UITextField, value: String?) {
        sender.text = presenter.format(baseCurrencyValue: value)
        presenter.didChangeBaseCurrency(value: sender.text)
    }

    //MARK: - CurrencyCalculatorView
    func show(baseCurrency: CurrencyCalculatorModel) {
        self.baseCurrency = baseCurrency
        tableView.reloadData()
    }

    func show(targets: [CurrencyCalculatorModel]) {
        targetsCurrencies = targets
        updateTargets(animated: true)
    }

    func updateTargets(animated: Bool) {
        if let index = tableData.firstIndex(where: { $0 == .targetCurrency }) {
            tableView.reloadSections([index], with: animated ? .automatic : .none)
        }
    }

    //MARK: - Table datasource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableData[section] {
        case .baseCurrency, .addTargetCurrency:
            return 1
        case .targetCurrency:
            return targetsCurrencies.count
        }
    }

    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
        -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive,
                                                  title: "DeleteKey".localized)
            { [weak self] _, _, _ in
                if let model = self?.targetsCurrencies.remove(at: indexPath.row)  {
                    self?.presenter.didTapDelete(target: model, at: indexPath.row)
                }
            }
            return UISwipeActionsConfiguration(actions: [deleteAction])
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return tableData[indexPath.section] == .targetCurrency
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = tableData[indexPath.section]

        let cell = tableView.dequeueReusableCell(withIdentifier: cellType.reuseIdentifier, for: indexPath)
        switch cellType {
        case .baseCurrency:
            let cell = cell as! EditBaseCurrencyCell
            let data = baseCurrency!
            cell.bind(symbol: data.symbol, value: data.value)
            cell.delegate = self
        case .targetCurrency:
            let cell = cell as! TargetCurrenecyCell
            let data = targetsCurrencies[indexPath.row]
            cell.bind(symbol: data.symbol, value: data.value)
        case .addTargetCurrency:
            cell.textLabel?.text = "AddNewTargetKey".localized
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = UIColor(red: 0.208, green: 0.471, blue: 0.965, alpha: 1)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellType = tableData[indexPath.section]
        switch cellType {
        case .targetCurrency:
            tableView.isEditing = !tableView.isEditing
            break
        case .addTargetCurrency:
            presenter.didTapNewTarget()
        default:
            break
        }
    }
}
