//
//  CurrencyNetworkGateway.swift
//  CurrencyConverter
//
//  Created by ilya on 02/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

protocol CurrencyNetworkGateway {
    
    func getAllCurrencies() -> Single<[String : String]>
    
    func getLatesRates(baseCurrencyKey: String?, targetsKeys: [String]?) -> Single<RateEntity>
}
