//
//  ApiConstants.swift
//  CurrencyConverter
//
//  Created by ilya on 01/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

final class ApiConstants {

    private init() {}

    class URLParameters {
        static let base = "base"
        static let symbols = "symbols"
        static let accessKey = "access_key"
    }

    class Path {
        static let symbols = "symbols"
        static let lates = "latest"
    }
}
