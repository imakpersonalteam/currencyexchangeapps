//
//  CurrencyApi.swift
//  CurrencyConverter
//
//  Created by ilya on 01/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import Moya

enum CurrencyApi {
    case getSymbols
    case getLatestRates(base: String?, targets: [String]?)

    static let provider = MoyaProvider<CurrencyApi>()
}

extension CurrencyApi: TargetType {

    var baseURL: URL {
        return URL(string: "http://data.fixer.io/api/")!
    }

    var path: String {
        switch self {
        case .getSymbols:
            return ApiConstants.Path.symbols
        case .getLatestRates:
            return ApiConstants.Path.lates
        }
    }

    var headers: [String : String]? {
        return nil
    }

    var task: Task {
        var urlParameters: [String : Any] = [ApiConstants.URLParameters.accessKey : "ebd8ca7e42a3eb3cc362ba55e4c1022d"]
        switch self {
        case .getSymbols:
            return .requestParameters(parameters: urlParameters, encoding: URLEncoding.default)
        case .getLatestRates(let base, let targets):
            if let base = base, !base.isEmpty {
                urlParameters[ApiConstants.URLParameters.base] = base
            }
            if let targets = targets, !targets.isEmpty {
                urlParameters[ApiConstants.URLParameters.symbols] = targets.joined(separator: ",")
            }
            return .requestParameters(parameters: urlParameters, encoding: URLEncoding.default)
        }
    }

    var method: Moya.Method {
        switch self {
        case .getSymbols, .getLatestRates:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }
}
