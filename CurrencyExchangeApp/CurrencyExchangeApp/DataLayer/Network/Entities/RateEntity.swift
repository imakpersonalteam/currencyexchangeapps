//
//  SymbolsEntity.swift
//  CurrencyConverter
//
//  Created by ilya on 01/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import Mapper

private let keyTimestamp = "timestamp"
private let keyBase = "base"
private let keyDate = "date"
private let keyRates = "rates"

struct RateEntity: Mappable {

    let timestamp: Int
    let baseCurrency: String
    let date: String
    let rates: [String : Double]

    init(map: Mapper) throws {
        timestamp = try map.from(keyTimestamp)
        baseCurrency = try map.from(keyBase)
        date = try map.from(keyDate)
        rates = try map.from(keyRates)
    }
}
