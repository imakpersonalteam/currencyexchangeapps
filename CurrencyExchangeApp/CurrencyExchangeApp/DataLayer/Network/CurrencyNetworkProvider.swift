//
//  CurrencyNetworkProvider.swift
//  CurrencyConverter
//
//  Created by ilya on 01/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift
import Moya_ModelMapper

class CurrencyNetworkProvider: CurrencyNetworkGateway {

    func getAllCurrencies() -> Single<[String : String]> {
        return CurrencyApi.provider.rx
            .request(.getSymbols)
            .filterSuccessfulStatusCodes()
            .map(Dictionary<String, String>.self, atKeyPath: "symbols")
    }

    func getLatesRates(baseCurrencyKey: String?, targetsKeys: [String]?) -> Single<RateEntity> {
        return CurrencyApi.provider.rx
            .request(.getLatestRates(base: baseCurrencyKey, targets: targetsKeys))
            .filterSuccessfulStatusCodes()
            .map(to: RateEntity.self)
    }
}
