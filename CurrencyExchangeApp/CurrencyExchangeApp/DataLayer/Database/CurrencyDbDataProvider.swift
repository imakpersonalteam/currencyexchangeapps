//
//  CurrencyDbDataProvider.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift
import CoreData

class CurrencyDbDataProvider: CurrencyDbGateway {

    private static let persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "CurrencyConverter")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    private func createSingle<T>(performContext: @escaping ((NSManagedObjectContext) -> T)) -> Single<T> {
        return Single.create { observer in
            CurrencyDbDataProvider.persistentContainer.performBackgroundTask { context in
                observer(.success(performContext(context)))
            }
            return Disposables.create()
        }
    }

    func currenciesCount() -> Single<Int> {
        return createSingle(performContext: { context -> Int in context.count(type: DbCurrency.self) })
    }

    func fetchAllCurrencies(filter: String?) -> Single<[Currency]> {
        return createSingle(performContext: { context -> [Currency] in
            let sortOption = NSSortDescriptor(key: #keyPath(DbCurrency.name),
                                              ascending: true,
                                              selector: #selector(NSString.caseInsensitiveCompare(_:)))
            var predicate: NSPredicate?
            if let filter = filter, !filter.isEmpty {
                let format = "\(#keyPath(DbCurrency.symbol)) CONTAINS[cd] %@ OR \(#keyPath(DbCurrency.name)) CONTAINS[cd] %@"
                predicate = NSPredicate(format: format, filter, filter)
            }
            return context.load(predicate: predicate, sort: [sortOption], type: DbCurrency.self)?
                .map { Currency(symbol: $0.symbol!, name: $0.name!) } ?? []
        })
    }

    func fetchSelectedCurrencyWithRates() -> Single<(base: Currency, targetsRates: [CurrencyRate])?> {
        return createSingle(performContext: { context -> (base: Currency, targetsRates: [CurrencyRate])? in
            let predicate = NSPredicate(format: "\(#keyPath(DbCurrencyRate.baseCurrency.isDefaultBaseCurrency)) == %@",
                NSNumber(booleanLiteral: true))
            let sortOption = NSSortDescriptor(keyPath: \DbCurrencyRate.symbol, ascending: true)
            if let dbRates = context.load(predicate: predicate,
                                          sort: [sortOption],
                                          type: DbCurrencyRate.self),
                !dbRates.isEmpty {
                let dbBaseCurrency = dbRates.first!.baseCurrency!
                let baseCurrency = Currency(symbol: dbBaseCurrency.symbol!, name: dbBaseCurrency.name!)
                let rates = dbRates.map { CurrencyRate(symbol: $0.symbol!, value: $0.value) }
                return (base: baseCurrency, targetsRates: rates)
            } else {
                return nil
            }
        })
    }

    func fetchRates(base: Symbol, targets: [Symbol]) -> Single<[CurrencyRate]> {
        return createSingle(performContext: { context -> [CurrencyRate] in
            let predicateBase = NSPredicate(format: "\(#keyPath(DbCurrencyRate.baseCurrency.symbol)) == %@", base)
            let predicateTargets = NSPredicate(format: "\(#keyPath(DbCurrencyRate.symbol)) IN %@", targets)
            let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateBase, predicateTargets])
            let sortOption = NSSortDescriptor(key: #keyPath(DbCurrencyRate.symbol),
                                              ascending: true,
                                              selector: #selector(NSString.caseInsensitiveCompare(_:)))
            return context.load(predicate: predicate, sort: [sortOption], type: DbCurrencyRate.self)?
                .map { CurrencyRate(symbol: $0.symbol!, value: $0.value) } ?? []
        })
    }

    func fetchTargetByBaseSymbols() -> Single<[Symbol : [Symbol]]> {
        return createSingle(performContext: { context -> [Symbol : [Symbol]] in
            var savedRatesByBaseCurrency = [String : [String]]()
            context.load(type: DbCurrencyRate.self)?.forEach {
                let baseSymbol = ($0.baseCurrency?.symbol)!
                if savedRatesByBaseCurrency[baseSymbol] != nil {
                    savedRatesByBaseCurrency[baseSymbol]?.append($0.symbol!)
                } else {
                    savedRatesByBaseCurrency[baseSymbol] = [$0.symbol!]
                }
            }
            return savedRatesByBaseCurrency
        })
    }

    func makeDefault(baseCurrency: Symbol) -> Single<Void> {
        return createSingle(performContext: { context -> Void in
            let currencies = context.load(type: DbCurrency.self)
            currencies?.forEach { currency in
                currency.isDefaultBaseCurrency = currency.symbol == baseCurrency
            }
            try? context.save()
        })
    }

    func delete(targetsRates: [Symbol], for baseCurrency: Symbol) -> Single<Void> {
        return createSingle(performContext: { context -> Void in
            let ratesForBasePr = NSPredicate(format: "\(#keyPath(DbCurrencyRate.baseCurrency.symbol)) == %@",
                baseCurrency)
            let ratesToDeletePr = NSPredicate(format: "\(#keyPath(DbCurrencyRate.symbol)) IN %@", targetsRates)
            let predicate = NSCompoundPredicate(type: .and, subpredicates: [ratesForBasePr, ratesToDeletePr])
            context.load(predicate: predicate, type: DbCurrencyRate.self)?.forEach { context.delete($0) }

            if context.hasChanges {
                try? context.save()
            }
        })
    }

    func merge<T>(currencies: [Symbol : String], mapper: T) -> Single<Void> where T : DbMapper,
        T.DbObject == DbCurrency,
        T.EntityType == (symbol: String, name: String) {
            return createSingle(performContext: { context -> Void in

                func insertNew(currencies: [Symbol : String]) {
                    currencies.forEach { sym, name in mapper.map(object: DbCurrency(context: context), from: (sym, name)) }
                }

                if context.count(type: DbCurrency.self) == 0 {
                    insertNew(currencies: currencies)
                } else {
                    var currencies = currencies
                    context.load(type: DbCurrency.self)?.forEach {
                        if currencies.removeValue(forKey: $0.symbol!) == nil {
                            context.delete($0)
                        }
                    }
                    insertNew(currencies: currencies)
                }
                try? context.save()
            })
    }

    func merge<T>(rates: [DbRateMergeEntity], for baseCurrency: Symbol, mapper: T) -> Single<Void> where T : DbMapper,
        T.DbObject == DbCurrencyRate,
        T.EntityType == DbRateMergeEntity {
            return createSingle(performContext: { context -> Void in
                let baseCurrencyPr = NSPredicate(format: "\(#keyPath(DbCurrency.symbol)) == %@", baseCurrency)
                if let baseCurrency = context.load(predicate: baseCurrencyPr, type: DbCurrency.self)?.first {
                    let savedRates = baseCurrency.targetRates?.allObjects as? [DbCurrencyRate]
                    var newRates = rates.toDictionary { $0.symbol }
                    savedRates?.forEach {
                        if let rate = newRates.removeValue(forKey: $0.symbol!) {
                            $0.value = rate.value
                            $0.timestamp = TimeInterval(rate.timestamp)
                        }
                    }
                    newRates.values.forEach {
                        let newRate = DbCurrencyRate(context: context)
                        mapper.map(object: newRate, from: $0)
                        newRate.baseCurrency = baseCurrency
                    }
                    try? context.save()
                }
            })
    }
}
