//
//  NSManagedObjectContextExtension.swift
//  CurrencyConverter
//
//  Created by ilya on 03/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext {

    func load<T:NSManagedObject>(predicate: NSPredicate? = nil,
                                 sort: [NSSortDescriptor]? = nil,
                                 limit: Int = 0,
                                 offset: Int = 0,
                                 type: T.Type) -> [T]? {
        let request = T.fetchRequest() as! NSFetchRequest<T>
        request.predicate = predicate
        request.sortDescriptors = sort
        request.fetchLimit = limit
        request.fetchOffset = offset
        return try? fetch(request)
    }

    func count<T:NSManagedObject>(predicate: NSPredicate? = nil, type: T.Type) -> Int {
        return (try? count(for: T.fetchRequest())) ?? 0
    }
}
