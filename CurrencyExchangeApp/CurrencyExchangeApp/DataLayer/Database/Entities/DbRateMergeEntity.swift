//
//  RateMergeEntity.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

class DbRateMergeEntity {

    let symbol: Symbol
    let value: Double
    let timestamp: TimeInterval

    init(symbol: Symbol, value: Double, timestamp: TimeInterval) {
        self.symbol = symbol
        self.value = value
        self.timestamp = timestamp
    }
}
