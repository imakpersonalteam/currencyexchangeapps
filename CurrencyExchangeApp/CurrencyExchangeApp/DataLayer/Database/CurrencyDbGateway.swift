//
//  CurrencyDbGateway.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

protocol CurrencyDbGateway {

    func currenciesCount() -> Single<Int>

    func fetchAllCurrencies(filter: String?) -> Single<[Currency]>

    func fetchSelectedCurrencyWithRates() -> Single<(base: Currency, targetsRates: [CurrencyRate])?>

    func fetchRates(base: Symbol, targets: [Symbol]) -> Single<[CurrencyRate]>

    func fetchTargetByBaseSymbols() -> Single<[Symbol: [Symbol]]>

    func makeDefault(baseCurrency: Symbol) -> Single<Void>

    func delete(targetsRates: [Symbol], for baseCurrency: Symbol) -> Single<Void>

    func merge<T: DbMapper>(currencies: [Symbol : String], mapper: T) -> Single<Void> where T.DbObject == DbCurrency,
        T.EntityType == (symbol: String, name: String)

    func merge<T: DbMapper>(rates: [DbRateMergeEntity], for baseCurrency: Symbol, mapper: T) -> Single<Void>
        where T.DbObject == DbCurrencyRate, T.EntityType == DbRateMergeEntity
}
