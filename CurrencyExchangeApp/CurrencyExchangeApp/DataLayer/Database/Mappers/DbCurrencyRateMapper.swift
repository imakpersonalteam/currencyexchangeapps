//
//  DbCurrencyRateMapper.swift
//  CurrencyConverter
//
//  Created by ilya on 06/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

class DbCurrencyRateMapper: DbMapper {

    typealias DbObject = DbCurrencyRate
    typealias EntityType = DbRateMergeEntity

    func map(object: DbObject, from entity: EntityType) {
        object.symbol = entity.symbol
        object.value = entity.value
        object.timestamp = entity.timestamp
    }
}
