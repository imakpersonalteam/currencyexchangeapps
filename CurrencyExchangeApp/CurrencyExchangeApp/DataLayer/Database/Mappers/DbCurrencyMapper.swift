//
//  DbCurrencyMapper.swift
//  CurrencyConverter
//
//  Created by ilya on 06/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

class DbCurrencyMapper: DbMapper {

    typealias DbObject = DbCurrency
    typealias EntityType = (symbol: String, name: String)

    func map(object: DbCurrency, from entity: (symbol: String, name: String)) {
        object.symbol = entity.symbol
        object.name = entity.name
    }
}
