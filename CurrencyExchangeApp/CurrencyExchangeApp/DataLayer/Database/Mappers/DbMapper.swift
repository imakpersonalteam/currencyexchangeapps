//
//  DbMapper.swift
//  CurrencyConverter
//
//  Created by ilya on 05/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

protocol DbMapper {

    associatedtype DbObject
    associatedtype EntityType

    func map(object: DbObject, from entity: EntityType)
}
