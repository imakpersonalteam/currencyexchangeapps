//
//  ArrayExtensions.swift
//  CurrencyConverter
//
//  Created by ilya on 08/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

extension Array {

    func toDictionary<T: Hashable>(by key: @escaping ((Element) -> T)) -> [T: Element] {
        var dict = [T:Element](minimumCapacity: count)
        forEach { dict[key($0)] = $0 }
        return dict
    }
}
