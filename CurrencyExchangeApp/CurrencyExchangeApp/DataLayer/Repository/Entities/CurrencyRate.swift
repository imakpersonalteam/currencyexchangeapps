//
//  CurrencyRate.swift
//  CurrencyConverter
//
//  Created by ilya on 02/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

struct CurrencyRate {
    let symbol: Symbol
    let value: Double
}
