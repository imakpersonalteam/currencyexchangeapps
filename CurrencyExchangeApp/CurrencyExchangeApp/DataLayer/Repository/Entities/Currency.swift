//
//  Currency.swift
//  CurrencyConverter
//
//  Created by ilya on 02/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation

typealias Symbol = String

struct Currency {
    let symbol: Symbol
    let name: String
}
