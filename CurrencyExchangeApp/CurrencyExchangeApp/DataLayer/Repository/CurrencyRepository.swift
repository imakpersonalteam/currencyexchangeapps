//
//  CurrencyRepository.swift
//  CurrencyConverter
//
//  Created by ilya on 02/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

protocol CurrencyRepository {

    func sync() -> Single<Void>

    func hasCurrencies() -> Single<Bool>

    func getDefaultCurrency() -> Single<(base: Currency, targetsRates: [CurrencyRate])?>

    func getAvailableCurrencies(filtered: String?) -> Single<[Currency]>

    func getRates(for base: Symbol, to targets: [Symbol]) -> Single<[CurrencyRate]>

    func makeDefault(baseCurrency: Symbol) -> Single<Void>

    func delete(targetsRates: [Symbol], for baseCurrency: Symbol) -> Single<Void>
}
