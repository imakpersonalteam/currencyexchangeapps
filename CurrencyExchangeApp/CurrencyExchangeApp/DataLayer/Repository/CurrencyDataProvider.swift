//
//  CurrencyDataProvider.swift
//  CurrencyConverter
//
//  Created by ilya on 02/06/19.
//  Copyright © 2019 ilya. All rights reserved.
//

import Foundation
import RxSwift

class CurrencyDataProvider: CurrencyRepository {

    private let dataLoader: CurrencyNetworkGateway
    private let dbLoader: CurrencyDbGateway
    
    init(dataLoader: CurrencyNetworkGateway, dbLoader: CurrencyDbGateway) {
        self.dataLoader = dataLoader
        self.dbLoader = dbLoader
    }
    
    func sync() -> Single<Void> {
        let loader = dataLoader
        let dbLoader = self.dbLoader
        return dbLoader.fetchTargetByBaseSymbols()
            .observeOn(ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global(qos: .background)))
            .flatMap { rates in

                // load only saved targets rates or load all targets for defult currency 'EUR' (subscription plan restriction)
                let loaders = !rates.isEmpty
                    ? rates.map { loader.getLatesRates(baseCurrencyKey: $0, targetsKeys: $1).asObservable() }
                    : [loader.getLatesRates(baseCurrencyKey: nil, targetsKeys: nil).asObservable()]

                return Observable<RateEntity>.zip(loaders)
                    .asSingle()
            }
            .flatMap { (updatedRates: [RateEntity]) in
                let mapper = DbCurrencyRateMapper()
                let mergeExecuters = updatedRates.map { entity -> Observable<Void> in
                    let rates = entity.rates.map { symbol, value in
                        DbRateMergeEntity(symbol: symbol, value: value, timestamp: TimeInterval(entity.timestamp))
                    }
                    return dbLoader.merge(rates: rates, for: entity.baseCurrency, mapper: mapper).asObservable()
                }
                return Observable.zip(mergeExecuters)
                    .asSingle()
                    .map { _ in }
        }
    }

    func hasCurrencies() -> Single<Bool> {
        return dbLoader.currenciesCount()
            .map { $0 > 0 }
    }

    func getAvailableCurrencies(filtered: String?) -> Single<[Currency]> {
        let dbLoader = self.dbLoader
        return dbLoader.fetchAllCurrencies(filter: filtered)
            .flatMap { [weak self] currencies in
                if currencies.isEmpty {
                    return self?.dataLoader.getAllCurrencies()
                        .flatMap { dbLoader.merge(currencies: $0, mapper: DbCurrencyMapper()) }
                        .flatMap { dbLoader.fetchAllCurrencies(filter: filtered) } ?? Single.just([])
                } else {
                    return Single.just(currencies)
                }
        }
    }

    func getDefaultCurrency() -> Single<(base: Currency, targetsRates: [CurrencyRate])?> {
        return dbLoader.fetchSelectedCurrencyWithRates()
    }
    
    func getRates(for base: Symbol, to targets: [Symbol]) -> Single<[CurrencyRate]> {
        let netLoader = dataLoader
        let dbLoader = self.dbLoader
        return dbLoader.fetchRates(base: base, targets: targets)
            .flatMap { rates in
                let savedRatesSymbols = rates.map { $0.symbol }
                var newTargetsSymbols = targets
                newTargetsSymbols.removeAll { savedRatesSymbols.contains($0) }
                if rates.isEmpty || !newTargetsSymbols.isEmpty {
                    return netLoader.getLatesRates(baseCurrencyKey: base, targetsKeys: newTargetsSymbols)
                        .flatMap { entity in
                            let rates = entity.rates.map { symbol, value in
                                DbRateMergeEntity(symbol: symbol,
                                                  value: value,
                                                  timestamp: TimeInterval(entity.timestamp))
                            }
                            return dbLoader.merge(rates: rates, for: base, mapper: DbCurrencyRateMapper())
                        }
                        .flatMap { dbLoader.fetchRates(base: base, targets: targets) }
                } else {
                    return Single.just(rates)
                }
        }
    }

    func makeDefault(baseCurrency: Symbol) -> Single<Void> {
        return dbLoader.makeDefault(baseCurrency: baseCurrency)
    }

    func delete(targetsRates: [Symbol], for baseCurrency: Symbol) -> Single<Void> {
        return dbLoader.delete(targetsRates: targetsRates, for: baseCurrency)
    }
}
